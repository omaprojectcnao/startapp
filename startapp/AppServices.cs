﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Windows.Foundation;
using Windows.Graphics.Display;
using Windows.Storage;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.Data.Xml.Dom;
using Windows.UI.Xaml.Media;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Shapes;
using Microsoft.Toolkit.Uwp.UI;

namespace Siprod.Services
{
    /// <summary>
    /// The class returns the preferred sizes of the startapp application
    /// </summary>
    public class AppConstants
    {
        /// <summary>
        /// The height of the startapp main window when initially started
        /// </summary>
        public const uint STARTAPP_HEIGHT = 52;
        /// <summary>
        /// The height iof the startapp window when the applications list is displayed
        /// </summary>
        public const uint STARTAPP_MAXHEIGHT = 650;
    }
    /// <summary>
    /// The class offers helper method valid for all applications
    /// </summary>
    public static class AppServices
    {
        /// <summary>
        /// Returns all the children Controls of a given Dependency Object
        /// </summary>
        /// <param name="parent">The Dependency Object the children Controls of which have to be returned</param>
        /// <param name="recurse"> if true also the grand-children and the grand grand-children and so on are returned</param>
        /// <returns>All the children of a given Dependency Object</returns>
        public static IEnumerable<DependencyObject> GetChildren(DependencyObject parent, bool recurse = true)
        {
            if (parent == null) yield break;
            var count = VisualTreeHelper.GetChildrenCount(parent);
            for (var i = 0; i < count; i++)
            {
                // Retrieve child visual at specified index value.
                var child = VisualTreeHelper.GetChild(parent, i) ;

                if (child == null) continue;
                yield return child;

                if (!recurse) continue;
                foreach (var grandChild in GetChildren(child))
                {
                    yield return grandChild;
                }
            }
        }
        /// <summary>
        /// The method has to be called in the OnLaunched method of the App class to resize the main application window
        /// </summary>
        /// <param name="width">the width of the main application window. If 0 then the width is the witdh of the screen reduced by 20 raw pixels</param>
        /// <param name="height">the height of the main application window. If 0 then the height is the height of the screen reduced by 20 raw pixels</param>
        public static void ResizeMainWindow(uint width, uint height)
        {
            if (width==0) 
                width= DisplayInformation.GetForCurrentView().ScreenWidthInRawPixels-20;
            if (height==0)
                width = DisplayInformation.GetForCurrentView().ScreenHeightInRawPixels-20;
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            var size = new Size(width * scaleFactor, height * scaleFactor);
            ApplicationView.GetForCurrentView().SetPreferredMinSize(size);
            ApplicationView.PreferredLaunchViewSize = size;
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
        }


        /// <summary>
        /// Tries to Resize the current view
        /// </summary>
        /// <param name="width">Requested width of the current view</param>
        /// <param name="height">Requested height of the current view</param>
        public static void ResizeCurrentView(double width, double height)
        {
            var view = ApplicationView.GetForCurrentView();
            if (width.Equals(0))
                width = view.VisibleBounds.Width;
            if (height.Equals(0))
                height = view.VisibleBounds.Height;
            view.TryResizeView(new Size { Width = width, Height = height });
        }
            private static readonly Dictionary<string, string> map = new Dictionary<string, string>();

            private static bool configurationDone;
        /// <summary>
        /// Reads the value of a setting contained in the file app.config and returns the value as a string.
        /// The app.config file must be located in the sub-directory assets\configuration in the executable directory.
        /// The format of the app.config file is the same as the forma of the app.config file in Windows Forms
        /// </summary>
        /// <param name="tag">name of the setting</param>
        /// <returns>The value of the setting</returns>
            public static string LookAtConf(string tag)
            {
            if (configurationDone) return map[tag];
            var file = StorageFile
                .GetFileFromApplicationUriAsync(
                    new Uri("ms-appx:///Assets/Configuration/app.config.xml")).GetAwaiter().GetResult();

            var xmlConfiguration
                = XmlDocument.LoadFromFileAsync(file).GetAwaiter().GetResult();
            var root = xmlConfiguration.DocumentElement;
            var nodes = root.SelectNodes("appSettings/add"); // You can also use XPath here

            foreach (var node in nodes.Cast<XmlElement>())
            {
                map.Add(node.Attributes[0].NodeValue.ToString(), node.Attributes[1].NodeValue.ToString());
            }
            configurationDone = true;
            return map[tag];
            }
        /// <summary>
        /// Serializes an object into an xml file
        /// </summary>
        /// <param name="list">is the object to serialize</param>
        /// <param name="folder">is the folder that shall contain the file with the serialized object</param>
        /// <param name="fileName">is the name of the xml file containing the serialized object</param>
        public static void SerializeList(object list, StorageFolder folder, string fileName)
        {
            var serializer = new XmlSerializer(list.GetType());


            using (var writer = new MemoryStream())
            {
                serializer.Serialize(writer, list);
                var file = folder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting).GetAwaiter().GetResult();
                var sr = new StreamReader(writer);
                sr.BaseStream.Position = 0;
                var myStr = sr.ReadToEnd();
                FileIO.WriteTextAsync(file, myStr).GetAwaiter().GetResult();
            }
        }
        /// <summary>
        /// De-serializes an object from an xml file
        /// </summary>
        /// <param name="folder">is the folder that contains the file with the serialized object</param>
        /// <param name="fileName">is the name of the xml file containing the serialized object</param>
        /// <param name="type">is the type of the object to de-serialize</param>
        /// <returns>Return the object that has been successfully de-serialized and transformed into a class istance</returns>
        public static object DeserializeList(StorageFolder folder, string fileName,Type type)
        {
            var serializer = new XmlSerializer(type);

            object list;
            using (var stream = new MemoryStream())
            {
                var file = (IStorageFile)folder.TryGetItemAsync(fileName).GetAwaiter().GetResult();
                file.OpenAsync(FileAccessMode.Read).GetAwaiter().GetResult();
                var str = FileIO.ReadTextAsync(file).GetAwaiter().GetResult();
                var writer = new StreamWriter(stream);
                writer.Write(str);
                writer.Flush();
                stream.Position = 0;
                list = serializer.Deserialize(stream);
            }
            return list;
        }
        /// <summary>
        /// Binds the property of a child FrameworkElement to the property of an instance of a class
        /// </summary>
        /// <param name="parent">The ancestor of the FrameworkElemen the property of which shall be bound</param>
        /// <param name="child">The name of the FrameworkElement the property of which shall be bound</param>
        /// <param name="childProperty"> the name of the FrameworkElement that shall be bound </param>
        /// <param name="instance">The class instance that contains the property to be bound</param>
        /// <param name="prop">The name of the property of the class instance to be bound</param>
        /// <returns>Returns true if successful, false otherwise</returns>
        public static bool BindElement(FrameworkElement parent, string child, string childProperty, object instance,
            string prop)
        {
            try
            {
                var control = parent.FindDescendantByName(child);
                control.DataContext = instance;
                var binding = new Binding { Path = new PropertyPath(prop) };
                control.SetBinding(Shape.FillProperty, binding);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
    /// <summary>
    /// The class is the base class for grouped ListViews ad GridViews
    /// </summary>
    public class GroupInfoList : ObservableCollection<object>
    {
        /// <summary>
        /// Contains the group key or name
        /// </summary>
        public object Key { get; set; }

    }

}
