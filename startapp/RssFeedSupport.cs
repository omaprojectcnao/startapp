﻿using System;
using System.Text.RegularExpressions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.Web.Syndication;

namespace startapp
{
    /// <summary>
    /// A class that allows reading contents of a given RSS feed in a GridView
    /// </summary>
    public class RssHelper
    {
        /// <summary>
        /// Loads the contents of an RSS feed in a GridView
        /// </summary>
        /// <param name="list">the GridView to be filled with the contents of the RSS feed</param>
        /// <param name="uri">the RSS feed Uri</param>
        public static async void Load(GridView list, Uri uri)
        {
            var client = new SyndicationClient();
            var feed = await client.RetrieveFeedAsync(uri);
            if (feed == null) return;
            foreach (var item in feed.Items)
            {
                var it = item;
                it.Summary.Text = StripHtml(item.Summary.Text);
                list.Items?.Add(it);
            }
        }
        /// <summary>
        /// Loads the contents of an RSS feed in a GridView or nothing if an exception is risen
        /// </summary>
        /// <param name="list">the GridView to be filled with the contents of the RSS feed</param>
        /// <param name="value">the RSS feed address (http://...)</param>
        public static void Go(ref GridView list, string value)
        {

            try
            {
                Load(list, new Uri(value));
            }
            catch
            {
                // ignored
            }
            list.Focus(FocusState.Keyboard);

        }
        private static string StripHtml(string source)
        {

            var output = Regex.Replace(source, "<[^>]*>", string.Empty);
            output = Regex.Replace(output, @"^\s*$\n", string.Empty, RegexOptions.Multiline);
            output = Regex.Replace(output, "&[^;]*;", string.Empty);


            output = output.Replace("\t", " ");
            output = output.Replace("  ", " ");
            output = output.Replace("FOTO", string.Empty);
            if (output.IndexOf("Parole chiave") > -1)
                output = output.Remove(output.IndexOf("Parole chiave"));
            return output;
        }
    }
}
