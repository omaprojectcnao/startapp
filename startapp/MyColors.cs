﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace startapp
{
    /// <summary>
    /// a test class that contains a property to be dynamically bound to a FreameworkEment property
    /// </summary>
    public class MyColors : INotifyPropertyChanged
    {
        private Brush myColor;
        /// <summary>
        /// Implements the Interface PropertyChanged event
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        /// <summary>
        /// Constructor
        /// </summary>
        public MyColors()
        {
            myColor = new SolidColorBrush(Colors.Blue);
        }
        /// <summary>
        /// The public property that can be bound to a FrameWorkElement property
        /// </summary>
        public Brush MyColor
        {
            get { return myColor; }
            set
            {
                myColor = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Raise the PropertyChanged event, passing the name of the property whose value has changed.
        /// </summary>
        /// <param name="propertyName">The name of the property that is going to be changed</param>
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
