﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Toolkit.Uwp.UI;

namespace startapp
{
    /// <summary>
    /// An extension class for the CommandBar that allows hiding the More Button (...)
    /// Adds the HideMoreButton property
    /// </summary>
    public static class CommandBarExtensions
    {
        /// <summary>
        /// Defines the boolean Dependency Property named  HideMoreButton
        /// </summary>
        public static readonly DependencyProperty HideMoreButtonProperty =
            DependencyProperty.RegisterAttached("HideMoreButton", typeof(bool), typeof(CommandBarExtensions),
                new PropertyMetadata(false, OnHideMoreButtonChanged));
        /// <summary>
        /// Returns the value of the HideMoreButton property
        /// </summary>
        /// <param name="element">The CommandBar owning the MoreButton</param>
        /// <returns>The value of the HideMoreButton property</returns>
        public static bool GetHideMoreButton(UIElement element)
        {
            if (element == null) throw new ArgumentNullException(nameof(element));
            return (bool)element.GetValue(HideMoreButtonProperty);
        }
        /// <summary>
        /// Sets the value of the HideMoreButton property
        /// </summary>
        /// <param name="element">The CommandBar owning the MoreButton</param>
        /// <param name="value">True if the MoreButton has to be hidden false otherwise</param>
        public static void SetHideMoreButton(UIElement element, bool value)
        {
            if (element == null) throw new ArgumentNullException(nameof(element));
            element.SetValue(HideMoreButtonProperty, value);
        }

        private static void OnHideMoreButtonChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var commandBar = d as CommandBar;
            if (e == null || commandBar == null || e.NewValue == null) return;
            var morebutton = commandBar.FindDescendantByName("MoreButton");
            if (morebutton != null)
            {
                var value = GetHideMoreButton(commandBar);
                morebutton.Visibility = value ? Visibility.Collapsed : Visibility.Visible;
            }
            else
            {
                commandBar.Loaded += CommandBarLoaded;
            }
        }

        private static void CommandBarLoaded(object o, object args)
        {
            var commandBar = o as CommandBar;
            var morebutton = commandBar?.FindDescendantByName("MoreButton");
            if (morebutton == null) return;
            var value = GetHideMoreButton(commandBar);
            morebutton.Visibility = value ? Visibility.Collapsed : Visibility.Visible;
            commandBar.Loaded -= CommandBarLoaded;
        }
    }
}
