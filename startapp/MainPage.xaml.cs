﻿using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using startapp.Models;
using Windows.ApplicationModel.DataTransfer;
using System.Collections.ObjectModel;
using Windows.ApplicationModel;
using Windows.UI.Popups;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Siprod.Services;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace startapp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {

        /// <summary>
        /// Constructor
        /// </summary>
        public MainPage()
        {
            InitializeComponent();
            if (DesignMode.DesignModeEnabled)
                return;
            AvailableApplication.ReadAvailableApps();
            Groups = AvailableApplication.Groups;
            AvailableApplication.GetApplicationsGrouped(Groups);
            AvailableApplication.ReadChosenApps();
            Apps = AvailableApplication.Apps;
        }


        private ObservableCollection<AppThumbnail> Apps { get; }

        private ObservableCollection<GroupInfoList> Groups { get; }


        private void SourceListView_DragItemsStarting(object sender, DragItemsStartingEventArgs e)
        {
            // Prepare a string with one dragged item per line
            foreach (var item in e.Items)
            {
                //if (items.Length > 0) items.AppendLine();
                //items.Append(item as string);
                e.Data.SetText(((AvailableApplication)item).Name);
            }
            // Set the content of the DataPackage

            // As we want our Reference list to stay intact, we only allow Copy
            e.Data.RequestedOperation = DataPackageOperation.Copy;
        }
        private void TargetListView_DragOver(object sender, DragEventArgs e)
        {
            // Our list only accepts text
            e.AcceptedOperation = (e.DataView.Contains(StandardDataFormats.Text)) ? DataPackageOperation.Copy : DataPackageOperation.None;
        }

        private async void TargetListView_Drop(object sender, DragEventArgs e)
        {
            if (!e.DataView.Contains(StandardDataFormats.Text)) return;
            var text = await e.DataView.GetTextAsync();
            AvailableApplication.AddChosenApp(text);
            e.AcceptedOperation = DataPackageOperation.Copy;
        }

        private async void It_Click(object sender, ItemClickEventArgs e)
        {
            var s= (AppThumbnail)(e.ClickedItem);
            await AvailableApplication.StartApp(s);
        }

        private void SearchBox_OnTextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            var view = ApplicationView.GetForCurrentView();
            if (view.VisibleBounds.Height.Equals(AppConstants.STARTAPP_HEIGHT))
            {
                AppServices.ResizeCurrentView(0, AppConstants.STARTAPP_MAXHEIGHT);
                RssHelper.Go(ref Display, "http://feeds.feedburner.com/skyittg24");
            }
            var autoSuggestBox = sender;
            var filtered = AvailableApplication.AvailableApps.Where(p => p.LastName.StartsWith(autoSuggestBox.Text)).Select(p => p.LastName).ToArray();
            autoSuggestBox.ItemsSource = filtered;
        }

        private async void SearchBox_OnQuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            var messageDialog = new MessageDialog("start Application: "+ args.QueryText);
            await messageDialog.ShowAsync();
        }

        private async void ListView_OnItemClick(object sender, ItemClickEventArgs e)
        {
            var a = (AvailableApplication)e.ClickedItem;
           await  AvailableApplication.ManageList(a);
           ListView.ScrollIntoView(a, ScrollIntoViewAlignment.Leading);
        }



        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            AvailableApplication.SaveChosenApps();
        }

        private void MainPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            var textcolor = new MyColors {MyColor = new SolidColorBrush(Colors.Coral)};
            AppServices.BindElement(this, "CloseEllipse", "Fill", textcolor, "MyColor");
            

        }

        private void MenuFlyoutItem_OnClick(object sender, RoutedEventArgs e)
        {
            var a = (MenuFlyoutItem)sender;
            var app = (AppThumbnail) a.DataContext;
            AvailableApplication.RemoveFromChosenApps(app);
        }


        private void Button_OnClick(object sender, RoutedEventArgs e)
        {
           
            AppServices.ResizeCurrentView(0, AppConstants.STARTAPP_MAXHEIGHT);
            RssHelper.Go(ref Display, "http://feeds.feedburner.com/skyittg24");
        }

        private void UIElement_OnTapped(object sender, TappedRoutedEventArgs e)
        {

            AppServices.ResizeCurrentView(0, AppConstants.STARTAPP_HEIGHT);
        }
    }
}
