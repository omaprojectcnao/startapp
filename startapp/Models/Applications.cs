﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Newtonsoft.Json;
using Windows.Storage;
using System.Threading.Tasks;
using Siprod.Services;
using Windows.System;
using Windows.UI.Popups;


namespace startapp.Models
{
    /// <summary>
    /// A class that describes a button representing an application
    /// </summary>
    public class AppThumbnail
    {
        /// <summary> a Symbol that is used to identify the type of application. 
        /// In the example can be Camera or Mail
        /// It is used to show different types of Mesage Boxes
        /// </summary>
        public Symbol Picture;
        /// <summary> The name of the application</summary>
        public string Name;
        /// <summary>The image associated to the application</summary>
        public BitmapImage Image;
    }
    /// <summary>
    /// A class that describes an application associating it o a category
    /// </summary>
    public class AppDescription
    {
        /// <summary> The name of the application</summary>
        public string Name;
        /// <summary> The category the application belongs to</summary>
        public string Category;
    }
    /// <summary>
    /// A class that is used to serialize the description of the available applications
    /// </summary>
    public class AppThumbnailWrapper
    {
        /// <summary>
        /// A list of <see cref="AppThumbnail"/> instances
        /// </summary>
        public AppThumbnail[] Apps;
    }
    /// <summary>
    /// The class that manages the available application definitions being shown in the Example
    /// </summary>
    public class AvailableApplication
    {
        #region Properties
        private string name;
        /// <summary>
        /// Returns the Available Application qualified name
        /// </summary>
        public string Name
        {
            get
            {
                if (name == string.Empty  && LastName != string.Empty)
                {
                    name = FirstName + " " + LastName;
                }
                return name;
            }
        }
        private string lastName;
        /// <summary>
        /// Returns the available application name
        /// </summary>
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
                name = string.Empty; // force to recalculate the value 
            }
        }
        private string firstName;
        /// <summary>
        /// Returns the available appliction qualifier
        /// </summary>
        private string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
                name = string.Empty; // force to recalculate the value 
            }
        }
        /// <summary>
        /// Ses or returns the string 'folder' or 'open' in case of a sub-directory. 
        /// Folder when the sub-directory contents are not present in the ListView
        /// Open when the dub-directory contents are  visible in the ListView
        /// </summary>
        public string Position { get; set; }
        /// <summary>
        /// Sets or returns the image assoiated with the available application
        /// </summary>
        public BitmapImage Image { get; set; }
        /// <summary>
        /// Sets or returns the Category to which an available application belongs to
        /// </summary>
        private string Category { get; set; }
        /// <summary>
        /// Sets or returns the indentation level of an available application in the ListView
        /// </summary>
        public string Level { get; set; }


        /// <summary>
        /// Contains the list of <see cref="AppThumbnail"/> instances representing all he available applications
        /// </summary>
        public static ObservableCollection<AppThumbnail> Apps=new ObservableCollection<AppThumbnail>();
        /// <summary>
        /// Contains the grouped list by category of <see cref="AvailableApplication"/> instances representing all he available applications
        /// </summary>
        public static ObservableCollection<GroupInfoList> Groups = new ObservableCollection<GroupInfoList>();
        /// <summary>
        /// A plain list of <see cref="AvailableApplication"/> instances
        /// </summary>
        public static ObservableCollection<AvailableApplication> AvailableApps = new ObservableCollection<AvailableApplication>();
        /// <summary>
        /// A plain list of <see cref="AppDescription"/> instances
        /// </summary>
        public static List<AppDescription> AppsDescription = new List<AppDescription>();
        /// <summary>
        /// An instance of  <see cref="AppThumbnailWrapper"/> used to serialize the buttons available in the CommandBar
        /// </summary>
        public static AppThumbnailWrapper AppsThumbnails=new AppThumbnailWrapper();
        #endregion
        private static Symbol presentSymbol = Symbol.Camera;
        private static BitmapImage presentImage;
        private static BitmapImage imageA;
        private static BitmapImage imageB;

        /// <summary>
        /// Constructor
        /// </summary>
        public AvailableApplication()
        {
            LastName = string.Empty;
            FirstName = string.Empty;
            Position = string.Empty;
        }

        #region Public Methods
        /// <summary>
        /// Saves the buttons representing available applications present in the CommandBar into a file named Apps.json
        /// The file is in Json format
        /// </summary>
        public static async void SaveChosenApps()
        {
            AppsThumbnails.Apps=new AppThumbnail[Apps.Count];
            Apps.CopyTo(AppsThumbnails.Apps,0);
            var data = JsonConvert.SerializeObject(AppsThumbnails);
            const string NAME = "Apps.json";
            var folder = ApplicationData.Current.LocalFolder;
            const CreationCollisionOption OPTION = CreationCollisionOption.ReplaceExisting;
            var file = await folder.CreateFileAsync(NAME, OPTION);
            await FileIO.WriteTextAsync(file, data);
        }
        /// <summary>
        /// Removes a Button representing an available application from the CommandBar
        /// </summary>
        /// <param name="app">The <see cref="AppThumbnail"/> representing the button to be removed  </param>
        public static void RemoveFromChosenApps(AppThumbnail app)
        {
            var i = 0;
            while (app.Name != Apps[i].Name)
                i++;
            Apps.RemoveAt(i);
        }
        /// <summary>
        /// Reads the available applications that have a button in the CommandBar from the file Apps.json
        /// The file is in Json format
        /// </summary>
        public static void ReadChosenApps()
        {

            const string NAME = "Apps.json";
            var folder = ApplicationData.Current.LocalFolder;
            var file = (IStorageFile)folder.TryGetItemAsync(NAME).GetAwaiter().GetResult();

            if (file == null)
                return;
            var str = FileIO.ReadTextAsync(file).GetAwaiter().GetResult();

            var wrapper = JsonConvert.DeserializeObject<AppThumbnailWrapper>(str);
            
            foreach (var s in wrapper.Apps)
            {
                var app = s;
                app.Image = FindApp(app.Name).Image;
                Apps.Add(app);
            }

        }
        /// <summary>
        /// Collapses a 'folder' in the ListView removing the contained application images
        /// </summary>
        /// <param name="a">The <see cref="AvailableApplication"/> representing the folder</param>
        public static void CloseFolder(AvailableApplication a)
        {
            var name = " " + a.LastName+" ";
            var stream = closedFolder.OpenAsync(FileAccessMode.Read).GetAwaiter().GetResult();
            var bitmapImage = new BitmapImage();
            bitmapImage.SetSource(stream);
            a.Image = bitmapImage;
            var q = (from app in AvailableApps where app.Name.StartsWith(name) select app).ToList();
            foreach (var app in q)
                AvailableApps.Remove(app);

        }
        /// <summary>
        /// Opens a 'folder' in the ListView adding the contained application images
        /// </summary>
        /// <param name="a">The <see cref="AvailableApplication"/> representing the folder</param>
        /// <returns>returns always true</returns>
        public static async Task<bool> OpenFolder(AvailableApplication a)
        {
            var path = AppServices.LookAtConf("ImagesShare");
            var folder =await StorageFolder.GetFolderFromPathAsync(path);
            var folders = await folder.GetFoldersAsync();

            var selectedFolder = folders.FirstOrDefault(f => f.Name == a.LastName);
            var files = await selectedFolder.GetFilesAsync();
            var stream = await openFolder.OpenAsync(FileAccessMode.Read);
            var bitmapImage = new BitmapImage();
            bitmapImage.SetSource(stream);
            a.Image = bitmapImage;
            foreach (var file in files)
            {
                stream = await file.OpenAsync(FileAccessMode.Read);

                bitmapImage = new BitmapImage();
                bitmapImage.SetSource(stream);
                var app = new AvailableApplication
                {
                    Image = bitmapImage,
                    FirstName = " " + a.LastName,
                    LastName = file.DisplayName,
                    Category = a.Category,
                    Level = "1"
                };
                AvailableApps.Add(app);

            }
            return true;
        }


        private static StorageFile openFolder;
        private static StorageFile closedFolder;
        /// <summary>
        /// Reads the available applications list from the folder defined with the tag ImagesShare in the configuration file app.config.xml
        /// </summary>
        public static  void ReadAvailableApps()
        {
            var path =AppServices.LookAtConf("ImagesShare");
            var folder =
                StorageFolder.GetFolderFromPathAsync(path).GetAwaiter().GetResult();
            var fileExists = false;
            var files = folder.GetFilesAsync().GetAwaiter().GetResult();


            foreach (var file in files)
            {
                if (file.DisplayName == "closed folder")
                    closedFolder = file;
                if (file.DisplayName == "open folder")
                    openFolder = file;
                if (file.FileType == ".xml")
                {
                    fileExists = true;
                    continue;
                }
                var stream =  file.OpenAsync(FileAccessMode.Read).GetAwaiter().GetResult();

                    var bitmapImage = new BitmapImage();
                bitmapImage.SetSource(stream);
                var app = new AvailableApplication
                {
                    Image = bitmapImage,
                    LastName = file.DisplayName,
                    Level = "0"
                };

                AvailableApps.Add(app);
                var appDesc = new AppDescription
                {
                    Name = app.Name,
                    Category = "no category"
                };
                AppsDescription.Add(appDesc);

            }
            var folders = folder.GetFoldersAsync().GetAwaiter().GetResult();
            var streamClosed = closedFolder.OpenAsync(FileAccessMode.Read).GetAwaiter().GetResult();
            foreach (var f in folders)
            {
                var bitmapImage = new BitmapImage();
                bitmapImage.SetSource(streamClosed);
                var app = new AvailableApplication
                {
                    Image = bitmapImage,
                    LastName = f.DisplayName,
                    Position = "folder",
                    Level = "0"
                };
                AvailableApps.Add(app);
            }
            if (!fileExists)
                AppServices.SerializeList(AppsDescription,folder,"categories.xml");
            AppsDescription.Clear();
            AppsDescription=(List<AppDescription>)AppServices.DeserializeList(folder, "categories.xml",typeof(List<AppDescription>));

            foreach (var app in AvailableApps)
            {
                var app1 = app;
                var result=AppsDescription.Find(x => x.Name == app1.LastName);
                app.Category = result!=null ? result.Category : "no category";
            }
            var q = (from app in AvailableApps where app.Category=="no category" select app).ToList();
            foreach (var app in q)
                AvailableApps.Remove(app);

        }
        /// <summary>
        /// Returns the <see cref="AvailableApplication"/> instance with the name given as parameter
        /// </summary>
        /// <param name="name">the name of the <see cref="AvailableApplication"/> instance to be returned</param>
        /// <returns>The requested <see cref="AvailableApplication"/> instance</returns>
        public static AvailableApplication FindApp(string name)
        {
            var n = name.Trim();
            return AvailableApps.FirstOrDefault(app => app.LastName == n);
        }


        /// <summary>
        /// Groups by category the list of application contained in the AvailableApps property
        /// </summary>
        /// <param name="groups">the variable containing the grouped applications</param>
        /// <returns>Returns the updated grouped list</returns>
        public static ObservableCollection<GroupInfoList> GetApplicationsGrouped(ObservableCollection<GroupInfoList> groups)
        {
            groups.Clear();


            var q = AvailableApps.OrderBy(a => a.Name);
            var query = from item in q
                        group item by item.Category into g
                        orderby g.Key
                        select new { GroupName = g.Key, Items = g  };


            foreach (var g in query)
            {
                var info = new GroupInfoList {Key = g.GroupName};

                foreach (var item in g.Items)
                {
                    info.Add(item);
                }
                groups.Add(info);
            }

            return groups;
        }
        /// <summary>
        /// Adda the dragged <see cref="AvailableApplication"/> instance to the list shown in the <see cref="CommandBar"/>
        /// </summary>
        /// <param name="text">the name of the <see cref="AvailableApplication"/> instance</param>
        public static void AddChosenApp(string text)
        {
            if (presentImage == null)
            {
                imageA = new BitmapImage();
                imageB = new BitmapImage();
                var uri = new Uri("ms-appx:///Assets/colori.bmp");
                imageA.UriSource = uri;
                uri = new Uri("ms-appx:///Assets/edit.png");
                imageB.UriSource = uri;
                presentImage = imageA;
            }
            var s = text;
            var items = s.Split('\n');
            foreach (var item in items)
            {
                var it = new AppThumbnail();
                var app = FindApp(item);
                it.Image = app.Image;
                it.Picture = presentSymbol;
                it.Name = item;
                Apps.Add(it);
                if (presentSymbol == Symbol.Camera)
                {
                    presentSymbol = Symbol.Mail;
                    presentImage = imageB;
                }
                else
                {
                    presentSymbol = Symbol.Camera;
                    presentImage = imageA;
                }
            }
        }
        /// <summary>
        /// Mimics starting an application when the user clicks an <see cref="AvailableApplication"/> Image on the <see cref="CommandBar"/>
        /// </summary>
        /// <param name="s">The <see cref="AppThumbnail"/> instance representing the clicked button</param>
        /// <returns>void</returns>
        public static  async Task StartApp(AppThumbnail s)
        {
            if (s.Picture == Symbol.Camera)
            {
                var messageDialog =
                    new MessageDialog(
                        "New updates have been found for this program. Would you like to install the new updates?",
                        "Updates available");

                // Add commands and set their command ids
                messageDialog.Commands.Add(new UICommand("Don't install", null, 0));
                messageDialog.Commands.Add(new UICommand("Install updates", null, 1));

                // Set the command that will be invoked by default
                messageDialog.DefaultCommandIndex = 1;

                // Show the message dialog and get the event that was invoked via the async operator
                var commandChosen = await messageDialog.ShowAsync();
                if (Convert.ToInt32(commandChosen.Id) == 1)
                {
                    var uri = new Uri("com.test.stacktest:");
                    await Launcher.LaunchUriAsync(uri);
                }

            }
            else
            {
                // Create the message dialog and set its content; it will get a default "Close" Button since there aren't any other buttons being added
                var messageDialog = new MessageDialog("A Mail has been sent");

                // Show the message dialog and wait
                await messageDialog.ShowAsync();
            }
        }
        /// <summary>
        /// Extends or shrinks the ListView or mimics starting an application according to the clicked image in the ListView
        /// </summary>
        /// <param name="a">The <see cref="AvailableApplication"/> Image that has been clicked</param>
        /// <returns>void</returns>
        public static async Task ManageList(AvailableApplication a)
        {
            switch (a.Position)
            {
                case "open":
                    CloseFolder(a);
                    GetApplicationsGrouped(Groups);


                    a.Position = "folder";
                    break;
                case "folder":
                    var ret = await OpenFolder(a);
                    if (ret)
                        GetApplicationsGrouped(Groups);
                    a.Position = "open";
                    break;
                default:
                    await ShowSignInDialogButton();
                    break;
            }
        }
        private static async Task<bool> ShowSignInDialogButton()
        {
            var signInDialog = new SignInContentDialog();
            await signInDialog.ShowAsync();

            switch (signInDialog.Result)
            {
                case SignInResult.SignInOk:
                    return true;
                case SignInResult.SignInFail:
                    return false;
                case SignInResult.SignInCancel:
                    return false;
            }
            return false;
        }

        #endregion

    }

}
