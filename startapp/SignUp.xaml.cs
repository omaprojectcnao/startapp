﻿// SignInContentDialog.xaml.cs

using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

namespace startapp
{
    /// <summary>
    /// An enumeration containing all the possible (faked) dialog results
    /// </summary>
    public enum SignInResult
    {
        /// <summary>The dialog closed successfully</summary>
        SignInOk,
        /// <summary>The dialog closed unsuccessfully</summary>
        SignInFail,
        /// <summary>The user canceled the dialog</summary>
        SignInCancel,
        /// <summary>No result is available</summary>
        Nothing
    }
    /// <summary>
    /// A class that shows a dialog box displayed when the user clicks an image contained in the available applications list
    /// </summary>
    public sealed partial class SignInContentDialog
    {
        /// <summary>
        /// Returns the result of the Dialog Box
        /// </summary>
        public SignInResult Result { get; private set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public SignInContentDialog()
        {
            InitializeComponent();
            Opened += SignInContentDialog_Opened;
            Closing += SignInContentDialog_Closing;
        }

        private async void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            // Ensure the user Name and password fields aren't empty. If a required field
            // is empty, set args.Cancel = true to keep the dialog open.
            if (string.IsNullOrEmpty(UserNameTextBox.Text))
            {
                args.Cancel = true;
                ErrorTextBlock.Text = "User Name is required.";
            }
            else if (string.IsNullOrEmpty(PasswordTextBox.Password))
            {
                args.Cancel = true;
                ErrorTextBlock.Text = "Password is required.";
            }

            // If you're performing async operations in the button click handler,
            // get a deferral before you await the operation. Then, complete the
            // deferral when the async operation is complete.

            var deferral = args.GetDeferral();
            if (await SomeAsyncSignInOperation())
            {
                Result = SignInResult.SignInOk;
            }
            else
            {
                Result = SignInResult.SignInFail;
            }
            deferral.Complete();
        }

        private async Task<bool> SomeAsyncSignInOperation()
        {
            return await Task.FromResult(true);
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            // User clicked Cancel.
            Result = SignInResult.SignInCancel;
        }

        private void SignInContentDialog_Opened(ContentDialog sender, ContentDialogOpenedEventArgs args)
        {
            Result = SignInResult.Nothing;

            // If the user Name is saved, get it and populate the user Name field.
            var roamingSettings = Windows.Storage.ApplicationData.Current.RoamingSettings;
            if (roamingSettings.Values.ContainsKey("userName"))
            {
                UserNameTextBox.Text = roamingSettings.Values["userName"].ToString();
                SaveUserNameCheckBox.IsChecked = true;
            }
        }

        private void SignInContentDialog_Closing(ContentDialog sender, ContentDialogClosingEventArgs args)
        {
            // If sign in was successful, save or clear the user Name based on the user choice.
            if (Result == SignInResult.SignInOk)
            {
                if (SaveUserNameCheckBox.IsChecked == true)
                {
                    SaveUserName();
                }
                else
                {
                    ClearUserName();
                }
            }

            // If the user entered a Name and checked or cleared the 'save user Name' checkbox, then clicked the back arrow,
            // confirm if it was their intention to save or clear the user Name without signing in. 
            if (Result != SignInResult.Nothing || string.IsNullOrEmpty(UserNameTextBox.Text)) return;
            if (SaveUserNameCheckBox.IsChecked == false)
            {
                args.Cancel = true;
                FlyoutBase.SetAttachedFlyout(this, (FlyoutBase)Resources["DiscardNameFlyout"]);
                FlyoutBase.ShowAttachedFlyout(this);
            }
            else if (SaveUserNameCheckBox.IsChecked == true && !string.IsNullOrEmpty(UserNameTextBox.Text))
            {
                args.Cancel = true;
                FlyoutBase.SetAttachedFlyout(this, (FlyoutBase)Resources["SaveNameFlyout"]);
                FlyoutBase.ShowAttachedFlyout(this);
            }
        }

        private void SaveUserName()
        {
            var roamingSettings = Windows.Storage.ApplicationData.Current.RoamingSettings;
            roamingSettings.Values["userName"] = UserNameTextBox.Text;
        }

        private void ClearUserName()
        {
            var roamingSettings = Windows.Storage.ApplicationData.Current.RoamingSettings;
            roamingSettings.Values["userName"] = null;
            UserNameTextBox.Text = string.Empty;
        }

        // Handle the button clicks from the flyouts.
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveUserName();
            FlyoutBase.GetAttachedFlyout(this).Hide();
        }

        private void DiscardButton_Click(object sender, RoutedEventArgs e)
        {
            ClearUserName();
            FlyoutBase.GetAttachedFlyout(this).Hide();
        }

        // When the flyout closes, hide the sign in dialog, too.
        private void Flyout_Closed(object sender, object e)
        {
            Hide();
        }
    }
}


